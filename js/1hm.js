// Теоретичні питання
// 1. Як можна оголосити змінну у Javascript?

//Змінну раніше оголошували через слово var, але цей метод нині застарів
//Зараз переважно використовують слово let для оголошення змінної
//Також одним з варіантів оголошення змінної є const, хоча тоді змінну не можна буде змінити далі за програмою


// 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?

//Рядок (string) - це поле з певним переліком символів, в якому може знаходитись текст у нашій програмі, а також змінні, які ми можемо записати і вивести прямо всередині цього рядку.
//Для його створення можна використати різні види лапок - "" '' ``
//Можна змінити один тип даних на рядок (використати конструктор string) і таким чином його створити


// 3. Як перевірити тип даних змінної в JavaScript?

//Для перевірки типу даних змінної треба використати слово typeof - це відразу вкаже нам в консолі до якого типу данних належить подана змінна.

// 4. Поясніть чому '1' + 1 = 11.
//'1' + 1 = 11 може бути у тому випадку, якщо ми робимо конкатенацію рядків - тобто "склеюємо" рядок "1" та число 1, таким чином отримуючи 11 у результаті
 


//ПРАКТИЧНІ ЗАВДАННЯ
//1. Оголосіть змінну і присвойте в неї число. Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

let num = 10;
console.log(Number.isInteger(num));

//2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
//Виведіть повідомлення у консоль у форматі `Мене звати {ім'я}, {прізвище}` використовуючи ці змінні.

let name = "Michail";
let lastName = "Budyonnii";
console.log(`Мене звати ${name}  ${lastName}!`);

//3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

let num2 = 12;
console.log(`Значення змінної: ${num2} `)
